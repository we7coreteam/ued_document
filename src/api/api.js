import axios from '@/utils/fetch'

export const createDoc = (data) => axios({
  url: '/admin/document/create',
  data,
  method: 'post'
})

export const createChapter = (data) => axios({
  url: '/admin/chapter/create',
  data,
  method: 'post'
})

// 获取请求方式列表
export const getMethodType = (params) => axios({
  url: '/admin/document/chapterapi/getApiLabel',
  params,
  method: 'get'
})

// 保存文档
export const saveChapter = (data) => axios({
  url: '/admin/chapter/save',
  data,
  method: 'post'
})

// 文档内容-查看
export const viewChapter = (data) => axios({
  url: '/admin/chapter/content',
  data,
  method: 'post'
})

// 获取所有项目
export const getAllProject = (data) => axios({
  url: '/admin/document/all',
  data,
  method: 'post'
})

// 获取项目内所有目录
export const getAllChapter = (data) => axios({
  url: '/admin/chapter/detail',
  data,
  method: 'post'
})

// 退出
export const logout = (data) => axios({
  url: '/common/auth/logout',
  data,
  method: 'post'
})

export const logouturl = (params) => axios({
  url: '/common/auth/getlogouturl',
  params,
  method: 'get'
})

// mock查看
export const mockDetail = (data) => axios({
  url: '/document/chapter/record',
  data,
  method: 'post'
})

// 系统检测
export const systemDetection = (data) => axios({
  url: '/install/systemDetection',
  data,
  method: 'post'
})

// 系统安装
export const install = (data) => axios({
  url: '/install/install',
  data,
  method: 'post'
})

// 系统安装检测
export const installConfig = (data) => axios({
  url: '/install/config',
  data,
  method: 'post'
})
//
export const getUser = (data) => axios({
  url: '/admin/user/all',
  data,
  method: 'post'
})

export const getSearchResults = (data) => axios({
  url: '/document/chapter/search',
  data,
  method: 'post'
})

export const getDocumentDetail = (data) => axios({
  url: '/document/detail',
  data,
  method: 'post'
})

// 删除文档
export const deleteDocument = (data) => axios({
  url: '/admin/document/delete',
  data,
  method: 'post'
})

// 第三方绑定账户、注册新用户
export const thirdPartyLoginBind = (data) => axios({
  url: '/common/auth/third-party-login-bind',
  data,
  method: 'post'
})

// 第三方绑定 切换账户
export const changeThirdPartyUser = (data) => axios({
  url: '/common/auth/changeThirdPartyUser',
  data,
  method: 'post'
})

export const bindThirdPartyUser = (data) => axios({
  url: '/common/auth/bindThirdPartyUser',
  data,
  method: 'post'
})

export const thirdPartyUserCacheIn = (data) => axios({
  url: '/common/auth/ThirdPartyUserCacheIn',
  data,
  method: 'post'
})

export const trySyncLogin = (data) => axios({
  url: 'https://api.w7.cc/oauth/authorize/try-sync-login',
  data,
  method: 'post'
})

// 解绑
export const unbind = (data) => axios({
  url: '/common/auth/unbind',
  data,
  method: 'post'
})

// 导入json
export const importJson = (data) => axios({
  url: '/admin/chapter/import ',
  data,
  method: 'post'
})

// 保存随机三条响应数据
export const saveResponseMockJson = (data) => axios({
  url: '/admin/document/chapterapi/setApiData',
  data,
  method: 'post'
})

// 是否有未读的反馈意见标识接口
export const getFeedFlag = (data) => axios({
  url: '/admin/document/new-feedback',
  data,
  method: 'post'
})
//敏感词设置
export const fbi = (data) => axios({
  url: '/admin/setting/save',
  data,
  method: 'post'
})
//获取屏蔽词
export const getForbinInfo = (params) => axios({
  url: '/admin/setting/config',
  params,
  method: 'get'
})
//获取用户个人资料
export const getUserInfo = (params) => axios({
  url: '/user/info',
  params,
  method: 'get'
})
//修改个人资料
export const editUserInfo = (data) => axios({
  url: '/user/update',
  data,
  method: 'post'
})
//获取用户个人动态
export const getUserOperate = (params) => axios({
  url: '/user/operate',
  params,
  method: 'get'
})
