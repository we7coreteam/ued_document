import axios from '@/utils/fetch'

// 反馈建议列表
export const getFeedList = (data) => axios({
  url: '/admin/document/feedback-list',
  data,
  method: 'post'
})

// 反馈建议详情
export const getFeedDetail = (data) => axios({
  url: '/admin/document/feedback-detail',
  data,
  method: 'post'
})

// 反馈意见提交
export const saveFeed = (data) => axios({
  url: '/document/feedback/add',
  data,
  method: 'post'
})

// 图片上传接口
export const uploadImage = (data) => axios({
  url: '/common/upload/image',
  data,
  method: 'post'
})
