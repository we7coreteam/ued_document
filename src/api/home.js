import axios from '@/utils/fetch'

// 获取首页设置数据
export const getHomeData = (params) => axios({
  url: '/document/home',
  params,
  method: 'get'
})

// 设置是否打开首页
export const setHomeOpen = (data) => axios({
  url: '/admin/home/set-open',
  data,
  method: 'post'
})

// 前端首页搜索接口
export const homeSearch = (data) => axios({
  url: '/document/home/search',
  data,
  method: 'post'
})

// 前端首页搜索接口
export const getHomeCheck = (data) => axios({
  url: '/document/home/check',
  data,
  method: 'get'
})

// 获取搜索热词列表
export const getHots = (data) => axios({
  url:'/document/home/search-hot',
  data,
  method:'get'
})



