import axios from '@/utils/fetch'

// 获取首页设置数据
export const getHomeData = (params) => axios({
  url: '/admin/home/get-set',
  params,
  method: 'get'
})

// 设置是否打开首页
export const setHomeOpen = (data) => axios({
  url: '/admin/home/set-open',
  data,
  method: 'post'
})

// 设置首页banner
export const setHomeBanner = (data) => axios({
  url: '/admin/home/set-banner',
  data,
  method: 'post'
})

// 设置首页名称
export const setHomeTitle = (data) => axios({
  url: '/admin/home/set-title',
  data,
  method: 'post'
})

// 首页文档设置列表
export const getHomeList = (params) => axios({
  url: '/admin/home/list',
  params,
  method: 'get'
})

// 首页文档设置-获取类型
export const getHomeType = (params) => axios({
  url: '/admin/home/get-type',
  params,
  method: 'get'
})

// 首页文档设置-模糊查询文档
export const homeSearchDoc = (data) => axios({
  url: '/admin/home/search-doc',
  data,
  method: 'post'
})

// 首页文档设置-添加
export const homeAdd = (data) => axios({
  url: '/admin/home/add',
  data,
  method: 'post'
})

// 首页文档设置-编辑
export const homeEdit = (data) => axios({
  url: '/admin/home/edit',
  data,
  method: 'post'
})

// 首页文档设置-删除操作
export const homeDelete = (data) => axios({
  url: '/admin/home/delete',
  data,
  method: 'post'
})

