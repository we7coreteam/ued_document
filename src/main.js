import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from './utils/fetch'
import ElementUI from 'element-ui';
import mavonEditor from 'mavon-editor'
import VueClipboard from 'vue-clipboard2'
import 'element-ui/lib/theme-chalk/index.css'
import 'mavon-editor/dist/css/index.css'
import './assets/scss/element-variables.scss'
import 'jquery'
import hljs from 'highlight.js'
import 'highlight.js/styles/vs.css' //样式文件

Vue.directive('highlight', {
  // 被绑定元素插入父节点时调用
  inserted: function(el) {
      let blocks = el.querySelectorAll('pre code');
      for (let i = 0; i < blocks.length; i++) {
        hljs.highlightBlock(blocks[i]);
      }
  },
  // 指令所在组件的 VNode 及其子 VNode 全部更新后调用
  componentUpdated: function(el) {
      let blocks = el.querySelectorAll('pre code');
      for (let i = 0; i < blocks.length; i++) {
        hljs.highlightBlock(blocks[i]);
      }
  }
})


let Mock = require('mockjs')
Vue.use(ElementUI)
Vue.use(mavonEditor)
Vue.use(VueClipboard)

Vue.prototype.$http = axios;
Vue.prototype.$post = axios.post;
Vue.prototype.$mock = Mock;
Vue.config.productionTip = false;

const vm = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

export default vm;

router.beforeEach((to, from, next) => {
  console.log('to');
  console.log(to);
  // console.log(from);
  // debugger
  if (to.name == 'adminLoginPage') {
    const recordHref = location.href;
    // const recordHref = from.name;
    let install = recordHref.indexOf('install');
    // console.log('install');
    // console.log(install);
    if (install < 0) {
      localStorage.recordHref = recordHref;
    }
  }
  next();
  // if (to.name === 'personalCenter') {
  //   next();
  // } else {
  //   next({name: 'personalCenter'});
  // }

  // setTimeout(() => {
  //   const recordHref = location.href;
  //   console.log(recordHref);
  //   if(recordHref.indexOf('admin-login') == -1) {
  //     localStorage.recordHref = recordHref;
  //   }
  // }, 500);
})
