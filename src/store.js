import Vue from "vue";
import Vuex from "vuex";
import Axios from "axios";
import { getAllProject, getUserInfo, getUserOperate } from "@/api/api";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    UserInfo: {},
    NavMenu: {},
    isSave: true,
    saveDialogVisible: false,
    allProjectListQuery: {
      page: 1,
      page_size: 15,
      role: "all"
    },
    userOperateListQuery: {
      user_id: 0,
      page: 1,
      page_size: 15
    },
    allProjectData: "",
    personInfo: "",
    userOperateData: ""
  },
  getters: {
    UserInfo(state) {
      return state.UserInfo;
    },
    NavMenu(state) {
      return state.NavMenu;
    }
  },
  mutations: {
    setUserInfo(state, data) {
      state.UserInfo = data;
    },
    setNavMenu(state, data) {
      state.NavMenu = data;
    },
    setAllProjectData(state, data) {
      state.allProjectData = data;
    },
    setPersonInfo(state, data) {
      state.personInfo = data;
    },
    setUserOperateData(state, data) {
      state.userOperateData = data;
    },
    setUserOperateListQuery(state, data) {
      Object.keys(data).map(key => {
        state.userOperateListQuery[key] = data[key];
      });
      console.log("state.userOperateListQuery");
      console.log(state.userOperateListQuery);
    }
  },
  actions: {
    getUserInfo(context) {
      return new Promise((resolve, reject) => {
        Axios.post("/common/auth/user")
          .then(res => {
            if (res.data.code == "444") {
              context.commit("setUserInfo", {
                has_privilege: "",
                username: ""
              });
            } else {
              context.commit("setUserInfo", res.data.data);
              context.commit("setUserOperateListQuery", {
                user_id: res.data.data.id
              });
              context.dispatch("getPersonInfo");
              context.dispatch("getUserOperate");
              resolve(res.data.data);
            }
          })
          .catch(e => {
            reject(e);
          });
      });
    },
    getNavMenu(context) {
      Axios.post("/menu/setting").then(res => {
        if (res.data.code == "444") {
          context.commit("setNavMenu", {
            theme: "",
            list: []
          });
        } else {
          context.commit("setNavMenu", res.data.data);
        }
      });
    },
    getAllProject({ commit, state }) {
      getAllProject(state.allProjectListQuery)
        .then(res => {
          if (res.code === 200) {
            commit("setAllProjectData", res.data);
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    getPersonInfo({ commit, state }) {
      getUserInfo({ username: state.UserInfo.username })
        .then(res => {
          commit("setPersonInfo", res.data);
        })
        .catch(e => {
          console.log(e);
        });
    },
    getUserOperate({ commit, state }) {
      getUserOperate(state.userOperateListQuery).then(res => {
        if (res.code === 200) {
          commit("setUserOperateData", res.data);
        }
      });
    }
  }
});
